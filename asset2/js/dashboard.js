(function ($) {
  'use strict';
  $(function () {
    if ($('#dashboard-area-chart').length) {
      var lineChartCanvas = $("#dashboard-area-chart").get(0).getContext("2d");
      var data = {
        labels: ["2013", "2014", "2014", "2015", "2016", "2017"],
        datasets: [{
            label: 'Product',
            data: [0, 11, 6, 10, 8, 0],
            backgroundColor: 'rgba(0, 128, 207, 0.4)',
            borderWidth: 1,
            fill: true
          },
          {
            label: 'Product',
            data: [0, 7, 11, 8, 11, 0],
            backgroundColor: 'rgba(2, 178, 248, 0.4)',
            borderWidth: 1,
            fill: true
          },
          {
            label: 'Support',
            data: [0, 14, 10, 14, 6, 0],
            backgroundColor: 'rgba(73, 221, 255, 0.4)',
            borderWidth: 1,
            fill: true
          }
        ]
      };
      var options = {
        responsive: true,
        maintainAspectRatio: true,
        scales: {
          yAxes: [{
            display: false
          }],
          xAxes: [{
            display: false,
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 3
          }
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        stepsize: 1
      };
      var lineChart = new Chart(lineChartCanvas, {
        type: 'line',
        data: data,
        options: options
      });
    }

    var bindDatePicker = function() {
     $(".date").datetimepicker({
         format:'YYYY-MM-DD',
       icons: {
         time: "fa fa-clock-o",
         date: "fa fa-calendar",
         up: "fa fa-arrow-up",
         down: "fa fa-arrow-down"
       }
     }).find('input:first').on("blur",function () {
       // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
       // update the format if it's yyyy-mm-dd
       var date = parseDate($(this).val());

       if (! isValidDate(date)) {
         //create date based on momentjs (we have that)
         date = moment().format('YYYY-MM-DD');
       }

       $(this).val(date);
     });
   }

    var isValidDate = function(value, format) {
     format = format || false;
     // lets parse the date to the best of our knowledge
     if (format) {
       value = parseDate(value);
     }

     var timestamp = Date.parse(value);

     return isNaN(timestamp) == false;
    }

    var parseDate = function(value) {
     var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
     if (m)
       value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

     return value;
    }

    bindDatePicker();
  });

  /*--------------------upload image--------------- */
    $(document).ready( function() {
       	$(document).on('change', '.btn-file :file', function() {
   		var input = $(this),
   			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
   		input.trigger('fileselect', [label]);
   		});

   		$('.btn-file :file').on('fileselect', function(event, label) {

   		    var input = $(this).parents('.input-group').find(':text'),
   		        log = label;

   		    if( input.length ) {
   		        input.val(log);
   		    } else {

   		    }

   		});
   		function readURL(input, id) {
   		    if (input.files && input.files[0]) {
   		        var reader = new FileReader();

   		        reader.onload = function (e) {
   		            $(id).attr('src', e.target.result);
   		        }

   		        reader.readAsDataURL(input.files[0]);
   		    }
   		}

   		$("#imgInp").change(function(){
   		    readURL(this, '#img-upload');
   		});

      $("#imgInp2").change(function(){
   		    readURL(this, '#img-upload2');
   		});
   	});



})(jQuery);
