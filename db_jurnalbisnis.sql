-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Feb 2019 pada 03.56
-- Versi server: 10.1.13-MariaDB
-- Versi PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_jurnalbisnis`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_kategori`
--

CREATE TABLE `data_kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL,
  `nama_toko` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_kategori`
--

INSERT INTO `data_kategori` (`id`, `nama_kategori`, `nama_toko`) VALUES
(13, 'Baju', ''),
(15, 'Celana', ''),
(4, 'Dress', ''),
(17, 'Jeans', ''),
(33, 'Jeanssss', ''),
(6, 'Kemeja', ''),
(16, 'Kerudung', ''),
(21, 'sepatu', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_order`
--

CREATE TABLE `data_order` (
  `nama_customer` varchar(50) NOT NULL,
  `kode_reseller` int(11) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `notlp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_order` date NOT NULL,
  `kode_produk` varchar(10) NOT NULL,
  `kategori_produk` varchar(50) NOT NULL,
  `foto_produk` text NOT NULL,
  `ukuran` varchar(20) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_produk`
--

CREATE TABLE `data_produk` (
  `id` int(11) NOT NULL,
  `kode_produk` varchar(10) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `gambar_produk` varchar(50) DEFAULT NULL,
  `kategori_produk` varchar(20) NOT NULL,
  `kode_supplier` int(11) NOT NULL,
  `ukuran` varchar(10) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `harga_jual_reseller` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_produk`
--

INSERT INTO `data_produk` (`id`, `kode_produk`, `nama_produk`, `gambar_produk`, `kategori_produk`, `kode_supplier`, `ukuran`, `harga_beli`, `harga_jual`, `harga_jual_reseller`, `stok`, `keterangan`, `tanggal`) VALUES
(4, '32145', 'Kemeja Zara', NULL, 'Kemeja', 0, 'XL', 35000, 45000, 43000, 7, 'Warna ungu', '2019-02-07'),
(3, '40536', 'Sweater Pull & Bear', NULL, 'Baju', 0, 'S', 275000, 299000, 289000, 5, 'Sweater warna navy dan dusty pink', '2019-02-04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_reseller`
--

CREATE TABLE `data_reseller` (
  `kode_reseller` int(11) NOT NULL,
  `nama_reseller` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `notlp` varchar(20) NOT NULL,
  `domisili` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_reseller`
--

INSERT INTO `data_reseller` (`kode_reseller`, `nama_reseller`, `alamat`, `notlp`, `domisili`) VALUES
(1, 'Denita Putri', 'Cijerah 2 No. 115', '084242424242', 'CImahi'),
(2, 'Rizka f', 'parmindo', '082323232323', 'CImahi'),
(3, 'Destryan Wiguna', 'Cijerah 2', '08882397970', 'Cimahi'),
(4, 'Denita Rizka', 'Cijerah Hegarmanah', '08882397970', 'Cimahi'),
(5, 'messi', 'Cijerah 2', '087656754444', 'Cimahi'),
(6, 'messi', 'Cijerah 2', '087656754444', 'Cimahi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_supplier`
--

CREATE TABLE `data_supplier` (
  `kode_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(50) NOT NULL,
  `notlp` varchar(20) NOT NULL,
  `kategori_produk` varchar(20) NOT NULL,
  `Alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_supplier`
--

INSERT INTO `data_supplier` (`kode_supplier`, `nama_supplier`, `notlp`, `kategori_produk`, `Alamat`) VALUES
(1, 'Denita Putri', '084545454545', 'Dress', 'CIjerah 2 Blok 17 No. 115'),
(2, 'Denita', '08882397970', 'Celana', 'Cijerah'),
(3, 'messi', '08882397970', 'Celana', 'cijerah 2'),
(4, 'messi', '08882397970', 'Celana', 'cijerah 2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`) VALUES
(3, 'denita_sunflo', '$2y$10$VeeFYARKLv53ZPWcYkdilus', 'Owner');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_karyawan`
--

CREATE TABLE `user_karyawan` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `nama_toko` varchar(50) NOT NULL,
  `foto` text NOT NULL,
  `notlp` varchar(20) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_owner`
--

CREATE TABLE `user_owner` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `nama_toko` varchar(50) NOT NULL,
  `notlp` varchar(20) NOT NULL,
  `email` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_owner`
--

INSERT INTO `user_owner` (`id`, `nama_lengkap`, `nama_toko`, `notlp`, `email`) VALUES
(0, 'Denita Putri', 'sunflo_store', '08882397970', 'denita117@gmail.com'),
(0, 'Denita Denita', 'sunflo_store', '08882397970', 'denita117@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_kategori`
--
ALTER TABLE `data_kategori`
  ADD PRIMARY KEY (`nama_kategori`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `data_order`
--
ALTER TABLE `data_order`
  ADD UNIQUE KEY `fk_kode` (`kode_produk`),
  ADD UNIQUE KEY `fk_kategori` (`kategori_produk`),
  ADD UNIQUE KEY `fk_kr` (`kode_reseller`);

--
-- Indeks untuk tabel `data_produk`
--
ALTER TABLE `data_produk`
  ADD PRIMARY KEY (`kode_produk`),
  ADD UNIQUE KEY `fk_kp` (`kategori_produk`,`kode_supplier`) USING BTREE,
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `data_reseller`
--
ALTER TABLE `data_reseller`
  ADD PRIMARY KEY (`kode_reseller`);

--
-- Indeks untuk tabel `data_supplier`
--
ALTER TABLE `data_supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_karyawan`
--
ALTER TABLE `user_karyawan`
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_kategori`
--
ALTER TABLE `data_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `data_produk`
--
ALTER TABLE `data_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `data_reseller`
--
ALTER TABLE `data_reseller`
  MODIFY `kode_reseller` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `data_supplier`
--
ALTER TABLE `data_supplier`
  MODIFY `kode_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_order`
--
ALTER TABLE `data_order`
  ADD CONSTRAINT `fk_kategori` FOREIGN KEY (`kategori_produk`) REFERENCES `data_kategori` (`nama_kategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kp` FOREIGN KEY (`kode_produk`) REFERENCES `data_produk` (`kode_produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kr` FOREIGN KEY (`kode_reseller`) REFERENCES `data_reseller` (`kode_reseller`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_karyawan`
--
ALTER TABLE `user_karyawan`
  ADD CONSTRAINT `fk_id` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
