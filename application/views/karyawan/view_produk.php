<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Produk | JB</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url() ?>asset2/images/favicon.png" />
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/iconfonts/font-awesome/css/font-awesome.css">
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <img src="<?php echo base_url() ?>asset2/images/logo.svg" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="<?php echo base_url() ?>asset2/images/logo-mini.svg" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">

        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
              <span class="count">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 3 new notifications
                </p>
                <span class="badge badge-pill badge-warning float-right">View all</span>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="mdi mdi-alert-circle-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Stock Habis!</h6>
                  <p class="font-weight-light small-text">
                    Stock Sweater Pull & Bear sudah habis!
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="mdi mdi-comment-text-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Stock Hampir Habis!</h6>
                  <p class="font-weight-light small-text">
                    Stock Dress Zara Hampir habis!
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="mdi mdi-email-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Barang Belum Dikirim!</h6>
                  <p class="font-weight-light small-text">
                    Ada 7 orderan belum dikirim!
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, Rizka Febriani !</span>
              <img class="img-xs rounded-circle" src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item mt-2">
                My Profile
              </a>
              <a class="dropdown-item">
                Setting
              </a>
              <a class="dropdown-item">
                Logout
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Rizka Febriani</p>
                  <div>
                    <small class="designation text-muted">Karyawan</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dashboard">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="order">
              <i class="menu-icon mdi mdi-cart"></i>
              <span class="menu-title">Order</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="produk">
              <i class="menu-icon mdi mdi-tshirt-crew"></i>
              <span class="menu-title">Produk</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="laporan">
              <i class="menu-icon mdi mdi-chart-line"></i>
              <span class="menu-title">Laporan Keuangan</span>
            </a>
          </li>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#setting" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-settings"></i>
              <span class="menu-title">Setting</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="setting">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link menu-icon mdi mdi-account" href="profil"> &nbsp; Profil </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link menu-icon mdi mdi-logout" href="logout.php"> &nbsp; Logout </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title text-primary mb-4">Data Semua Produk</h2>
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#home">Data Produk</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#menu1">Tambah</a>

                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#menu2">Kategori Produk</a>
                    </li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div id="home" class="container tab-pane active"><br>
                      <div class="col-lg-12">
                        <div class="row">
                          <div class="col-lg-2 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                              <div class="text-center">
                                <p class="mt-1">Jumlah Produk</p>
                                <h3 class="font-weight-medium mb-0">1</h3>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-2 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                              <div class="text-center">
                                <p class="mt-1">Jumlah Kategori</p>
                                <h3 class="font-weight-medium mb-0">5</h3>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-2 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                              <div class="text-center">
                                <p class="mt-1">Jumlah Sisa Stock</p>
                                <h3 class="font-weight-medium mb-0">0</h3>
                              </div>
                            </div>
                          </div>

                          <div class="form-group col-lg-4">
                            <label class="form-label">Kategori</label>
                              <select class="form-control" name="kategori">
                                <option>Sweater</option>
                                <option>Kemeja</option>
                                <option>Celana</option>
                                <option>Atasan</option>
                              </select>

                          </div>
                        </div>

                        <div class="row">
                          <div class="form-group col-lg-12">
                              <table class="table table-bordered table-hover" id="tabel-produk">
                                <thead>
                                  <tr class="text-center">
                                    <th style="width:30px;"> No </th>
                                    <th> Foto </th>
                                    <th> Nama Produk </th>
                                    <th> Ukuran </th>
                                    <th> Harga Beli </th>
                                    <th> Harga Jual </th>
                                    <th> Stock </th>
                                    <th> Kategori </th>
                                    <th style="width:50px;"> Opsi </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    $no = 1;
                                    foreach($data_produk as $tp) :  ?>

                                  <tr class="text-center">
                                    <td> <?php echo $no++ ?> </td>
                                    <td class="py-1">
                                      <img src="<?php echo base_url() ?>asset2/images/Produk/dress.jpg" alt="image" />
                                    </td>
                                    <td> <?php echo $tp->nama_produk ?> </td>
                                    <td> <?php echo $tp->ukuran ?> </td>
                                    <td> <?php echo $tp->harga_beli ?> </td>
                                    <td> <?php echo $tp->harga_jual ?> </td>
                                    <td> <?php echo $tp->stok ?> </td>
                                    <td> <?php echo $tp->kategori_produk ?> </td>
                                    <td>
                                      <a href="<?= base_url(); ?>index.php/jbkaryawan/detailProduk/<?= $tp->id ?>" class="btn social-btn btn-social-outline-facebook"data-toggle="modal" data-target="#modalproduk<?= $tp->id ?>" title="Edit/Lihat" style="padding:5px;">
                                        <i class="mdi mdi-magnify-plus"></i></a>
                                      <a href="<?= base_url(); ?>index.php/jbkaryawan/hapus_produk/<?= $tp->id ?>" class="btn social-btn btn-social-outline-facebook" style="padding:5px;">
                                        <i class="mdi mdi-delete"></i></a>


                                      <!-- The Modal -->
                                      <div class="modal fade" id="modalproduk<?= $tp->id ?>">
                                        <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                              <h4 class="modal-title">Form Edit Produk</h4>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <form class="form-sample" method="POST" action="">
                                              <!-- Modal body -->
                                              <div class="modal-body text-left">
                                                <div class="col-md-12">
                                                  <div class="row">

                                                    <div class="col-md-2">
                                                      <div class="form-group row" style="margin-left:20%;">
                                                        <img src="<?php echo base_url() ?>asset2/images/Produk/package.png" id='img-upload'/>
                                                        <span class="input-group-btn">
                                                          <span class="btn btn-default btn-file">
                                                            Pilih Gambar <input type="file" id="imgInp">
                                                          </span>
                                                        </span>
                                                      </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                      <label class="form-label">Nama Produk</label>
                                                      <input type="text" class="form-control" name="nama_produk" value="<?= $tp->nama_produk ?>"/>
                                                      <label class="form-label">Ukuran</label>
                                                      <input type="text" class="form-control" name="ukuran" value="<?= $tp->ukuran ?>"/>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                      <label class="form-label">Kategori</label>

                                                      <select class="form-control" name="kategori_produk" value="<?= $tp->kategori_produk ?>">
                                                        <?php
                                                          foreach($data_kategori as $dt) :  ?>
                                                        <option> <?php echo $dt->nama_kategori ?>  </option>
                                                        <?php endforeach; ?>
                                                      </select>
                                                      <label class="form-label">Kode</label>
                                                      <input type="text" class="form-control" name="kode_produk" value="<?= $tp->kode_produk ?>"/>
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                      <label [cla]ss="form-label">Harga Beli / Buat</label>
                                                      <input type="number" min="0" class="form-control" name="harga_beli" value="<?= $tp->harga_beli ?>"/>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                      <label class="form-label">Harga Jual Normal</label>
                                                      <input type="number" min="0" class="form-control" name="harga_jual" value="<?= $tp->harga_jual ?>"/>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                      <label class="form-label">Harga Jual Reseller</label>
                                                      <input type="number" min="0" class="form-control" name="harga_jual_reseller" value="<?= $tp->harga_jual_reseller ?>"/>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                      <label class="form-label">Stok</label>
                                                      <input type="number" min="0" class="form-control" name="stok" value="<?= $tp->stok ?>"/>
                                                    </div>
                                                    <div class="form-group col-md-8">
                                                      <label for="exampleTextarea1">Keterangan</label>
                                                      <textarea class="form-control" id="exampleTextarea1" rows="2" name="keterangan"><?= $tp->keterangan ?></textarea>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <!-- Modal footer -->
                                              <div class="modal-footer">
                                                <button type="submit" class="btn btn-success mr-2">Simpan</button>
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>

                                    </td>
                                  </tr>
                                  <?php endforeach; ?>
                                </tbody>
                              </table>
                          </div>
                        </div>
                      </div>
                    </div>

                            <div id="menu1" class="container tab-pane fade"><br>
                                  <form class="form-sample" method="POST" action="proses_produk">
                                    <h4 class="card-title text-primary">Tambah Produk</h4>
                                    <div class="col-md-12">
                                      <div class="row">
                                        <div class="col-md-2">
                                          <div class="form-group row">
                                            <img src="<?php echo base_url() ?>asset2/images/Produk/package.png" id='img-upload2'/>
                                            <span class="input-group-btn">
                                              <span class="btn btn-default btn-file">
                                                Pilih Gambar <input type="file" id="imgInp2">
                                              </span>
                                            </span>
                                          </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label class="form-label">Nama Produk</label>
                                          <input type="text" class="form-control" name="nama_produk"/>
                                          <label class="form-label">Ukuran</label>
                                          <input type="text" class="form-control" name="ukuran"/>
                                        </div>
                                        <div class="form-group col-md-4">
                                          <label class="form-label">Kategori</label>
                                          <select class="form-control" name="kategori_produk">

                                            <?php
                                              foreach($data_kategori as $tp) :  ?>
                                            <option> <?php echo $tp->nama_kategori ?>  </option>
                                            <?php endforeach; ?>


                                          </select>
                                          <label class="form-label">Kode</label>
                                          <input type="text" class="form-control" name="kode_produk"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="form-group col-md-3">
                                        <label [cla]ss="form-label">Harga Beli / Buat</label>
                                        <input type="number" min="0" class="form-control" name="harga_beli"/>
                                      </div>
                                      <div class="form-group col-md-3">
                                        <label class="form-label">Harga Jual Normal</label>
                                        <input type="number" min="0" class="form-control" name="harga_jual"/>
                                      </div>
                                      <div class="form-group col-md-3">
                                        <label class="form-label">Harga Jual Reseller</label>
                                        <input type="number" min="0" class="form-control" name="harga_jual_reseller"/>
                                      </div>
                                      <div class="form-group col-md-3">
                                        <label class="form-label">Stok</label>
                                        <input type="number" min="0" class="form-control" name="stok"/>
                                      </div>
                                      <div class="form-group col-md-8">
                                        <label for="exampleTextarea1">Keterangan</label>
                                        <textarea class="form-control" id="exampleTextarea1" rows="2" name="keterangan"></textarea>
                                      </div>
                                      <div class="form-group col-md-4">
                                        <label class="form-label">Tanggal</label>
                                        <input type="date" class="form-control" id="datepicker1" name="tanggal"/>
                                      </div>
                                      <div class="form-group col-md-3">
                                        <button type="submit" class="btn btn-success mr-2">Simpan</button>
                                      </div>
                                    </div>
                                  </form>
                                </div>

                                  <div id="menu2" class="container tab-pane fade"><br>
                                    <div class="col-md-12">
                                      <div class="row">
                                        <div class="form-group col-md-2">
                                          <button type="button" class="btn btn-outline-primary btn-fw" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-tag"></i>Tambah Kategori</button>
                                            <!-- The Modal -->
                                            <div class="modal fade" id="myModal">
                                              <div class="modal-dialog modal-md">
                                                <div class="modal-content">
                                                  <!-- Modal Header -->
                                                  <div class="modal-header">
                                                    <h4 class="modal-title">Form Tambah Kategori</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  </div>

                                                  <form class="form-sample" method="POST" action="proses_kategori">
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                      <label class="form-label">Nama Kategori</label>
                                                      <input type="text" class="form-control" name="nama_kategori"/>
                                                    </div>

                                                    <!-- Modal footer -->
                                                    <div class="modal-footer">
                                                      <button type="submit" class="btn btn-secondary">Save</button>
                                                    </div>
                                                  </form>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                      <div class="row">
                                        <div class="form-group col-md-12">
                                          <div class="table-responsive">
                                            <table class="table table-bordered table-hover" id="tabel-kategori">
                                              <thead>
                                                <tr class="text-center">
                                                  <th style="width:50px;"> No </th>
                                                  <th> Kategori </th>
                                                  <th style="width:50px;"> Opsi </th>
                                                </tr>
                                              </thead>

                                              <tbody>
                                                <?php  $no = 1;
                                                foreach($data_kategori as $u) : ?>
                                                <tr>
                                                  <td class="text-center"> <?php echo $no++ ?> </td>
                                                  <td> <?php echo $u->nama_kategori ?></td>
                                                  <td class="text-center">
                                                    <a href="<?= base_url(); ?>index.php/jbkaryawan/hapus_kategori/<?= $u->id ?>" class="btn social-btn btn-social-outline-facebook" style="padding:5px;">
                                                      <i class="mdi mdi-delete"></i></a>
                                                  </td>
                                                </tr>
                                              <?php endforeach; ?>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                </form>
                              </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo base_url() ?>asset2/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url() ?>asset2/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->

  <!-- inject:js -->
  <script src="<?php echo base_url() ?>asset2/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>asset2/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url() ?>asset2/js/dashboard.js"></script>
  <!-- End custom js for this page-->

  <script src="<?php echo base_url() ?>asset2/js/bootstrap-datepicker.js"></script>

  <script>

  $(function () {
    $('#tabel-produk').DataTable()
    $('#tabel-kategori').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })


  });

</script>
</body>

</html>
