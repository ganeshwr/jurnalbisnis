<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Laporan Keuangan | JB</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->

  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url() ?>asset2/images/favicon.png" />
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/iconfonts/font-awesome/css/font-awesome.css">

  <!--amchart-->
  <script type="text/javascript" src="<?php echo base_url() ?>asset2/chart/amcharts.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>asset2/chart/serial.js"></script>

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <img src="<?php echo base_url() ?>asset2/images/logo.svg" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="<?php echo base_url() ?>asset2/images/logo-mini.svg" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">

        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
              <span class="count">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 3 new notifications
                </p>
                <span class="badge badge-pill badge-warning float-right">View all</span>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="mdi mdi-alert-circle-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Stock Habis!</h6>
                  <p class="font-weight-light small-text">
                    Stock Sweater Pull & Bear sudah habis!
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="mdi mdi-comment-text-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Stock Hampir Habis!</h6>
                  <p class="font-weight-light small-text">
                    Stock Dress Zara Hampir habis!
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="mdi mdi-email-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Barang Belum Dikirim!</h6>
                  <p class="font-weight-light small-text">
                    Ada 7 orderan belum dikirim!
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, Rizka Febriani !</span>
              <img class="img-xs rounded-circle" src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item mt-2">
                My Profile
              </a>
              <a class="dropdown-item">
                Setting
              </a>
              <a class="dropdown-item">
                Logout
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Rizka Febriani</p>
                  <div>
                    <small class="designation text-muted">Karyawan</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dashboard">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="order">
              <i class="menu-icon mdi mdi-cart"></i>
              <span class="menu-title">Order</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="produk">
              <i class="menu-icon mdi mdi-tshirt-crew"></i>
              <span class="menu-title">Produk</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="laporan">
              <i class="menu-icon mdi mdi-chart-line"></i>
              <span class="menu-title">Laporan Keuangan</span>
            </a>
          </li>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#setting" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-settings"></i>
              <span class="menu-title">Setting</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="setting">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link menu-icon mdi mdi-account" href="profil"> &nbsp; Profil </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link menu-icon mdi mdi-logout" href="logout.php"> &nbsp; Logout </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h2 class="col md-12 card-title text-primary mb-4">Laporan Keuangan</h2>

                    <div class="wrapper d-flex justify-content-between">
                      <div class="col md-5 side-left">
                        <p class="mb-2">Pemasukan Tahun Ini</p>
                        <p class="display-3 mb-1 font-weight-light">Rp 3.640.000</p>
                      </div>
                      <div class="side-right" style="margin-right:5%;">
                        <small class="text-muted">2018</small>
                      </div>

                      <div class="col md-5 side-left">
                        <p class="mb-2">Pengeluaran Tahun Ini</p>
                        <p class="display-3 mb-1 font-weight-light">Rp 2.100.000</p>
                      </div>
                      <div class="side-right" style="margin-right:5%;">
                        <small class="text-muted">2018</small>
                      </div>

                      <div class="form-group col-md-2">
                        <label class="form-label">Tahun</label>
                          <select class="form-control" name="kategori">
                            <option>2018</option>
                            <option>2019</option>
                          </select>
                      </div>
                    </div>

                  <div class="col-md-12">
                    <div class="row">
                        <div id="chartdiv" style="width: 100%; height: 400px;"></div>
                    </div>
                  </div>

                  <div class="form-group text-center col-md-12">
                    <p class="display-5 mb-1 font-weight-light">Pemasukan</p>
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr class="text-center">
                            <th style="width:30px;"> No </th>
                          <th> Nama Pembeli </th>
                          <th> Nama Produk </th>
                          <th> Ukuran </th>
                          <th> Harga </th>
                          <th> Qty </th>
                          <th> Total </th>
                          <th> Tanggal </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="text-center">
                          <td> 1 </td>
                          <td> Risye Nurfarah </td>
                          <td> Tokichoi Colourblock Sweater</td>
                          <td> M </td>
                          <td> Rp 380.000 </td>
                          <td> <input type="number" class="text-center form-control" name="qty" value="2"/></td>
                          <td> Rp 760.000 </td>
                          <td> 23-01-2018 </td>
                        </tr>
                        <tr class="text-center">
                          <td> 2 </td>
                          <td> Widia Sari </td>
                          <td> Tokichoi Colourblock Sweater</td>
                          <td> XL </td>
                          <td> Rp 380.000 </td>
                          <td> <input type="number" class="text-center form-control" name="qty" value="3"/></td>
                          <td> Rp 1.080.000 </td>
                          <td> 05-02-2018 </td>
                        </tr>
                        <tr class="text-center">
                          <td> 3 </td>
                          <td> Resti Meita </td>
                          <td> Tokichoi Colourblock Sweater</td>
                          <td> S </td>
                          <td> Rp 380.000 </td>
                          <td> <input type="number" class="text-center form-control" name="qty" value="5"/></td>
                          <td> Rp 1.800.000 </td>
                          <td> 13-03-2018 </td>
                        </tr>
                      </tbody>
                    </table>

                    <div class="text-right col-md-12 template-demo">
                      <div class="btn-group " role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-primary"><<</button>
                        <button type="button" class="btn btn-primary">1</button>
                        <button type="button" class="btn btn-primary">2</button>
                        <button type="button" class="btn btn-primary">3</button>
                        <button type="button" class="btn btn-primary">>></button>
                      </div>
                    </div>

                    <p class="display-5 mb-1 mt-5 font-weight-light">Pengeluaran</p>

                    <table class="table table-bordered table-hover" id="tabel-pengeluaran">
                      <thead>
                        <tr class="text-center">
                            <th style="width:30px;"> No </th>
                          <th> Nama Produk </th>
                          <th> Ukuran </th>
                          <th> Harga </th>
                          <th> Qty </th>
                          <th> Total </th>
                          <th> Keterangan </th>
                          <th> Tanggal </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $no = 1;
                          foreach($data_produk as $tp) :  ?>

                        <tr class="text-center">
                          <td> <?php echo $no++ ?> </td>
                          <td> <?php echo $tp->nama_produk ?> </td>
                          <td> <?php echo $tp->ukuran ?> </td>
                          <td> <?php echo $tp->harga_beli ?> </td>
                          <td> <input type="number" class="text-center form-control" name="qty" value="2"/></td>
                          <td> ini total 700.000 </td>
                          <td> <?php echo $tp->keterangan ?> </td>
                          <td> <?php echo $tp->tanggal ?> </td>
                        </tr>

                      <?php endforeach; ?>

                      </tbody>
                    </table>

                </div>

              </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo base_url() ?>asset2/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url() ?>asset2/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?php echo base_url() ?>asset2/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>asset2/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url() ?>asset2/js/dashboard.js"></script>
  <!-- End custom js for this page-->

  <script type="text/javascript" id="jasons">
  $(function () {

    $('#tabel-pengeluaran').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })


  });

          var chart;
           var chartData = [
               {
                   "year": "Januari",
                   "income": 760000,
                   "expenses": 700000
               },
               {
                   "year": "Februari",
                   "income": 1080000,
                   "expenses": 1050000
               },
               {
                   "year": "Maret",
                   "income": 1800000,
                   "expenses": 1750000
               },
               {
                   "year": "April",
                   "income": 0,
                   "expenses": 0
               },
               {
                   "year": "Mei",
                   "income": 0,
                   "expenses": 0
               },
               {
                   "year": "Juni",
                   "income": 0,
                   "expenses": 0
               },
               {
                   "year": "Juli",
                   "income": 0,
                   "expenses": 0
               },
               {
                   "year": "Agustus",
                   "income": 0,
                   "expenses": 0
               },
               {
                   "year": "September",
                   "income": 0,
                   "expenses": 0
               },
               {
                   "year": "Oktober",
                   "income": 0,
                   "expenses": 0
               },
               {
                   "year": "November",
                   "income":0,
                   "expenses": 0
               },
               {
                   "year": "Desember",
                   "income": 0,
                   "expenses": 0
               }

           ];

           AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();

                chart.dataProvider = chartData;
                chart.categoryField = "year";
                chart.startDuration = 1;

                chart.handDrawn = true;
                chart.handDrawnScatter = 3;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridPosition = "start";

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);

                // GRAPHS
                // column graph
                var graph1 = new AmCharts.AmGraph();
                graph1.type = "column";
                graph1.title = "Pemasukan";
                graph1.lineColor = "#3da2e1";
                graph1.valueField = "income";
                graph1.lineAlpha = 1;
                graph1.fillAlphas = 1;
                graph1.dashLengthField = "dashLengthColumn";
                graph1.alphaField = "alpha";
                graph1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
                chart.addGraph(graph1);

                // line
                var graph2 = new AmCharts.AmGraph();
                graph2.type = "line";
                graph2.title = "Pengeluaran";
                graph2.lineColor = "#fcd202";
                graph2.valueField = "expenses";
                graph2.lineThickness = 3;
                graph2.bullet = "round";
                graph2.bulletBorderThickness = 3;
                graph2.bulletBorderColor = "#fcd202";
                graph2.bulletBorderAlpha = 1;
                graph2.bulletColor = "#ffffff";
                graph2.dashLengthField = "dashLengthLine";
                graph2.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
                chart.addGraph(graph2);

                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.useGraphSettings = true;
                chart.addLegend(legend);

                // WRITE
                chart.write("chartdiv");
            });

      $(document).ready(function() {
        $('select').material_select();

      });

    </script>
</body>

</html>
