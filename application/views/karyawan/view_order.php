<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Order | JB</title>
	<!-- plugins:css -->
	<link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/css/vendor.bundle.base.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/css/vendor.bundle.addons.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="<?php echo base_url() ?>asset2/css/style.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>asset2/images/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/iconfonts/font-awesome/css/font-awesome.css">
</head>

<body>
	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
			<div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
				<a class="navbar-brand brand-logo" href="index.html">
					<img src="<?php echo base_url() ?>asset2/images/logo.svg" alt="logo" />
				</a>
				<a class="navbar-brand brand-logo-mini" href="index.html">
					<img src="<?php echo base_url() ?>asset2/images/logo-mini.svg" alt="logo" />
				</a>
			</div>
			<div class="navbar-menu-wrapper d-flex align-items-center">

				<ul class="navbar-nav navbar-nav-right">
					<li class="nav-item dropdown">
						<a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
							<i class="mdi mdi-bell"></i>
							<span class="count">3</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
							<a class="dropdown-item">
								<p class="mb-0 font-weight-normal float-left">You have 3 new notifications
								</p>
								<span class="badge badge-pill badge-warning float-right">View all</span>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item preview-item">
								<div class="preview-thumbnail">
									<div class="preview-icon bg-success">
										<i class="mdi mdi-alert-circle-outline mx-0"></i>
									</div>
								</div>
								<div class="preview-item-content">
									<h6 class="preview-subject font-weight-medium text-dark">Stock Habis!</h6>
									<p class="font-weight-light small-text">
										Stock Sweater Pull & Bear sudah habis!
									</p>
								</div>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item preview-item">
								<div class="preview-thumbnail">
									<div class="preview-icon bg-warning">
										<i class="mdi mdi-comment-text-outline mx-0"></i>
									</div>
								</div>
								<div class="preview-item-content">
									<h6 class="preview-subject font-weight-medium text-dark">Stock Hampir Habis!</h6>
									<p class="font-weight-light small-text">
										Stock Dress Zara Hampir habis!
									</p>
								</div>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item preview-item">
								<div class="preview-thumbnail">
									<div class="preview-icon bg-info">
										<i class="mdi mdi-email-outline mx-0"></i>
									</div>
								</div>
								<div class="preview-item-content">
									<h6 class="preview-subject font-weight-medium text-dark">Barang Belum Dikirim!</h6>
									<p class="font-weight-light small-text">
										Ada 7 orderan belum dikirim!
									</p>
								</div>
							</a>
						</div>
					</li>
					<li class="nav-item dropdown d-none d-xl-inline-block">
						<a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
							<span class="profile-text">Hello, Rizka Febriani !</span>
							<img class="img-xs rounded-circle" src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" alt="Profile image">
						</a>
						<div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
							<a class="dropdown-item mt-2">
								My Profile
							</a>
							<a class="dropdown-item">
								Setting
							</a>
							<a class="dropdown-item">
								Logout
							</a>
						</div>
					</li>
				</ul>
				<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
					<span class="mdi mdi-menu"></span>
				</button>
			</div>
		</nav>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<!-- partial:partials/_sidebar.html -->
			<nav class="sidebar sidebar-offcanvas" id="sidebar">
				<ul class="nav">
					<li class="nav-item nav-profile">
						<div class="nav-link">
							<div class="user-wrapper">
								<div class="profile-image">
									<img src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" alt="profile image">
								</div>
								<div class="text-wrapper">
									<p class="profile-name">Rizka Febriani</p>
									<div>
										<small class="designation text-muted">Karyawan</small>
										<span class="status-indicator online"></span>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="dashboard">
							<i class="menu-icon mdi mdi-television"></i>
							<span class="menu-title">Dashboard</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="order">
							<i class="menu-icon mdi mdi-cart"></i>
							<span class="menu-title">Order</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="produk">
							<i class="menu-icon mdi mdi-tshirt-crew"></i>
							<span class="menu-title">Produk</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="laporan">
							<i class="menu-icon mdi mdi-chart-line"></i>
							<span class="menu-title">Laporan Keuangan</span>
						</a>
					</li>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="collapse" href="#setting" aria-expanded="false" aria-controls="auth">
							<i class="menu-icon mdi mdi-settings"></i>
							<span class="menu-title">Setting</span>
							<i class="menu-arrow"></i>
						</a>
						<div class="collapse" id="setting">
							<ul class="nav flex-column sub-menu">
								<li class="nav-item">
									<a class="nav-link menu-icon mdi mdi-account" href="profil"> &nbsp; Profil </a>
								</li>
								<li class="nav-item">
									<a class="nav-link menu-icon mdi mdi-logout" href="logout.php"> &nbsp; Logout </a>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</nav>
			<!-- partial -->
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-lg-12 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<h2 class="card-title text-primary mb-4">Data Semua Order</h2>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#home">Data Orderan</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#menu1">Tambah Order</a>
										</li>
									</ul>

									<!-- Tab panes -->
									<div class="tab-content">
										<div id="home" class="container tab-pane active"><br>
											<div class="col-lg-12">
												<div class="row">
													<div class="col-lg-2 col-md-3 col-sm-6 grid-margin stretch-card">
														<div class="card card-statistics">
															<div class="text-center">
																<p class="mt-1">Jumlah Orderan</p>
																<h3 class="font-weight-medium mb-0">3</h3>
															</div>
														</div>
													</div>

													<div class="col-lg-2 col-md-3 col-sm-6 grid-margin stretch-card">
														<div class="card card-statistics">
															<div class="text-center">
																<p class="mt-1">Belum Lunas</p>
																<h3 class="font-weight-medium mb-0">1</h3>
															</div>
														</div>
													</div>

													<div class="col-lg-2 col-md-3 col-sm-6 grid-margin stretch-card">
														<div class="card card-statistics">
															<div class="text-center">
																<p class="mt-1">Belum Dikirim</p>
																<h3 class="font-weight-medium mb-0">1</h3>
															</div>
														</div>
													</div>

													<div class="col-lg-2 col-md-3 col-sm-6 grid-margin stretch-card">
														<div class="card card-statistics">
															<div class="text-center">
																<p class="mt-1">Sudah Dikirim</p>
																<h3 class="font-weight-medium mb-0">2</h3>
															</div>
														</div>
													</div>

													<div class="form-group col-md-4">
														<label class="form-label">Filter</label>
														<select class="form-control" name="kategori">
															<option>Semua Orderan</option>
															<option>Belum Lunas</option>
															<option>Sudah Lunas</option>
															<option>Sudah Dikirim</option>
														</select>
													</div>

													<div class="form-group col-md-12">
														<div class="input-group">
															<div class="input-group-prepend bg-primary border-primary">
																<span class="input-group-text bg-transparent">
																	<i class="fa fa-search text-white"></i>
																</span>
															</div>
															<input type="text" class="form-control" placeholder="Search" aria-label="search">
														</div>
													</div>

												</div>

												<div class="row">
													<div class="form-group col-lg-12">
														<table class="table table-bordered table-hover">
															<thead>
																<tr class="text-center">
																	<th style="width:30px;"> No </th>
																	<th> Nama Pembeli </th>
																	<th> Gambar </th>
																	<th> Produk </th>
																	<th> Ukuran </th>
																	<th> Harga </th>
																	<th> Qty </th>
																	<th> Total </th>
																	<th> Keterangan </th>
																	<th style="width:50px;"> Opsi </th>
																</tr>
															</thead>
															<tbody>
																<tr class="text-center">
																	<td> 1 </td>
																	<td> Risye Nurfarah </td>
																	<td class="py-1">
																		<img src="<?php echo base_url() ?>asset2/images/Produk/dress.jpg" alt="image" />
																	</td>
																	<td> Tokichoi Colourblock Sweater Dress </td>
																	<td> M </td>
																	<td> 380.000 </td>
																	<td> 2 </td>
																	<td> 767.000 </td>
																	<td> Sudah Dikirim </td>
																	<td>
																		<button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px; margin-right:5px;"
																		 data-toggle="modal" data-target="#modalorder" title="Lihat/Edit">
																			<i class="mdi mdi-magnify-plus"></i> </button>

																		<!-- The Modal -->
																		<div class="modal fade" id="modalorder">
																			<div class="modal-dialog modal-lg">
																				<div class="modal-content">
																					<!-- Modal Header -->
																					<div class="modal-header">
																						<h4 class="modal-title">Form Edit Orderan </h4>
																						<button type="button" class="close" data-dismiss="modal">&times;</button>
																					</div>

																					<form class="form-sample" method="POST" action="proses_kategori">
																						<!-- Modal body -->
																						<div class="modal-body text-left">
																							<div class="col-md-12">
																								<div class="row">
																									<div class="form-group col-md-4">
																										<label class="form-label">Nama Pembeli</label>
																										<input type="text" class="form-control" name="nmcustomer" />
																										<label class="form-label">No Tlp</label>
																										<input type="number" class="form-control" name="notlp" />
																									</div>

																									<div class="form-group col-md-4">
																										<label for="exampleTextarea1">Alamat</label>
																										<textarea class="form-control" rows="6" name="alamat"></textarea>
																									</div>

																									<div class="form-group col-md-4">
																										<label class="form-label">Tanggal Order</label>
																										<input type="date" class="form-control" name="tglorder" />

																										<div class="form-radio">
																											<label class="form-check-label">
																												<input type="radio" class="form-check-input" name="lunas" value="Lunas" checked> Lunas
																											</label>
																										</div>
																										<div class="form-radio">
																											<label class="form-check-label">
																												<input type="radio" class="form-check-input" name="blunas" value="Belum Lunas"> Belum Lunas
																											</label>
																										</div>
																									</div>

																									<div class="form-group col-md-12">
																										<label class="form-label">Cari Produk</label>
																										<div class="input-group mb-2">
																											<div class="input-group-prepend bg-primary border-primary">
																												<span class="input-group-text bg-transparent">
																													<i class="fa fa-search text-white"></i>
																												</span>
																											</div>
																											<input type="text" class="form-control" placeholder="Search" aria-label="search">
																										</div>

																										<table class="table table-bordered table-hover">
																											<thead>
																												<tr class="text-center">
																													<th style="width:30px;"> No </th>
																													<th> Gambar </th>
																													<th> Nama Produk </th>
																													<th> Ukuran </th>
																													<th> Keterangan </th>
																													<th> Harga </th>
																													<th> Kuantitas </th>
																													<th> Total </th>
																													<th style="width:50px;"> Opsi </th>
																												</tr>
																											</thead>
																											<tbody>
																												<tr class="text-center">
																													<td> 1 </td>
																													<td class="py-1">
																														<img src="<?php echo base_url() ?>asset2/images/Produk/dress.jpg" alt="image" />
																													</td>
																													<td> Tokichoi Colourblock Sweater</td>
																													<td> M </td>
																													<td> Warna Navy </td>
																													<td> 380.000 </td>
																													<td> <input type="number" class="form-control" name="qty" /></td>
																													<td> 760.000 </td>
																													<td>
																														<button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px; margin-right:5px;"
																														 data-toggle="tooltip" title="Edit">
																															<i class="mdi mdi-delete"></i>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</div>

																									<div class="form-group col-md-4 mt-2">
																										<label class="form-label">Biaya Ongkos Kirim</label>
																										<input type="number" class="form-control" name="ongkir" />
																									</div>
																									<div class="form-group col-md-2">
																										<h4 class="card-title text-center text-primary mb-3">Total Bayar</h4>
																										<h2> 767.000 </h2>
																									</div>
																									<div class="col-md-2">
																										<h4 class="card-title text-primary mb-3">Keterangan</h4>
																										<div class="form-radio">
																											<label class="form-check-label">
																												<input type="radio" class="form-check-input" name="sudahkirim" checked> Sudah Dikirim
																											</label>
																										</div>
																										<div class="form-radio">
																											<label class="form-check-label">
																												<input type="radio" class="form-check-input" name="bkirim"> Belum Dikirim
																											</label>
																										</div>
																									</div>

																								</div>
																							</div>
																						</div>

																						<!-- Modal footer -->
																						<div class="modal-footer">
																							<!--    <button type="button" class="btn btn-outline-info btn-fw">
                                                  <i class="mdi mdi-printer"></i>Print</button> !-->
																							<button type="submit" class="btn btn-success mr-2">Simpan</button>
																						</div>
																					</form>
																				</div>
																			</div>
																		</div>

																		<button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px;"
																		 data-toggle="tooltip" title="Hapus">
																			<i class="mdi mdi-delete"></i>
																	</td>
																</tr>

																<tr class="text-center">
																	<td> 2 </td>
																	<td> Widia Sari </td>
																	<td class="py-1">
																		<img src="<?php echo base_url() ?>asset2/images/Produk/dress.jpg" alt="image" />
																	</td>
																	<td> Tokichoi Colourblock Sweater Dress </td>
																	<td> XL </td>
																	<td> 360.000 </td>
																	<td> 3 </td>
																	<td> 1.080.000 </td>
																	<td> Belum Dikirim </td>
																	<td>
																		<button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px; margin-right:5px;"
																		 data-toggle="modal" data-target="#modalorder" title="Lihat/Edit">
																			<i class="mdi mdi-magnify-plus"></i> </button>
																		<button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px;"
																		 data-toggle="tooltip" title="Hapus">
																			<i class="mdi mdi-delete"></i>
																</tr>

																<tr class="text-center">
																	<td> 3 </td>
																	<td> Resti Meita </td>
																	<td class="py-1">
																		<img src="<?php echo base_url() ?>asset2/images/Produk/dress.jpg" alt="image" />
																	</td>
																	<td> Tokichoi Colourblock Sweater Dress </td>
																	<td> S </td>
																	<td> 360.000 </td>
																	<td> 5 </td>
																	<td> 1.800.000 </td>
																	<td> Sudah Dikirim </td>
																	<td>
																		<button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px; margin-right:5px;"
																		 data-toggle="modal" data-target="#modalorder" title="Lihat/Edit">
																			<i class="mdi mdi-magnify-plus"></i> </button>
																		<button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px;"
																		 data-toggle="tooltip" title="Hapus">
																			<i class="mdi mdi-delete"></i>
																</tr>

															</tbody>
														</table>

														<div class="text-right col-md-12 template-demo">
															<div class="btn-group " role="group" aria-label="Basic example">
																<button type="button" class="btn btn-primary">
																	<<</button> <button type="button" class="btn btn-primary">1
																</button>
																<button type="button" class="btn btn-primary">2</button>
																<button type="button" class="btn btn-primary">3</button>
																<button type="button" class="btn btn-primary">>></button>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>

										<!-- Tambah Order -->
										<div id="menu1" class="container tab-pane fade"><br>
											<!-- <form action="tambah_order" method="post"> -->
												<div class="row">
													<div class="form-group col-lg-4 grid-margin">
														<h4 class="card-title text-primary mb-3">Data Customer</h4>
														<label class="form-label">Nama</label>
														<input id="nama_customer" type="text" class="form-control" name="nmcustomer" />
														<label class="form-label">No Tlp</label>
														<input id="notlp" type="number" class="form-control" name="notlp" />
														<label for="exampleTextarea1">Alamat</label>
														<textarea id="alamat" class="form-control" rows="4" name="alamat"></textarea>
														<label class="form-label">Tanggal Order</label>
														<input id="tgl_order" type="date" class="form-control mb-4" name="tglorder" />

														<h4 class="card-title text-primary mb-3">Keterangan Produk</h4>
														<textarea id="keterangan" class="form-control" rows="4" name="keterangan"></textarea>
													</div>

													<div class="col-lg-8 grid-margin">
														<div class="form-group card">
															<h2 class="card-title text-primary mb-3">Produk</h2>
															<label class="form-label">Cari Produk</label>
															<div class="input-group">
																<div class="input-group-prepend bg-primary border-primary">
																	<span class="input-group-text bg-transparent">
																		<i class="fa fa-search text-white"></i>
																	</span>
																</div>
																<input id="search_produk" type="text" class="form-control" placeholder="Search" aria-label="search">
															</div>
														</div>

														<div class="form-group card">
															<table class="table table-bordered table-hover mb-2">
																<thead>
																	<tr class="text-center">
																		<th style="width:30px;"> No </th>
																		<th> Gambar </th>
																		<th> Nama ( Ukuran )</th>
																		<th> Harga </th>
																		<th> Kuantitas </th>
																		<th> Total </th>
																		<th style="width:50px;"> Opsi </th>
																	</tr>
																</thead>
																<tbody id="hasil_search_tp">
																	<tr class="text-center">
																		<td colspan="7"> Kosong </td>
																	</tr>
																	<!-- <tr class="text-center">
                                  <td> 1 </td>
                                  <td class="py-1">
                                    <img src="<?php echo base_url() ?>asset2/images/Produk/dress.jpg" alt="image" />
                                  </td>
                                  <td> Tokichoi Colourblock Sweater M</td>
                                  <td> 380.000 </td>
                                  <td> <input type="number" class="form-control" name="qty"/></td>
                                  <td> 760.000 </td>
                                  <td>
                                    <button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px; margin-right:5px;" data-toggle="tooltip" title="Edit">
                                    <i class="mdi mdi-delete"></i>
                                  </td>
                                </tr> -->
																</tbody>
															</table>
															<label class="form-label">Biaya Ongkos Kirim</label>
															<input id="ongkir" type="number" class="form-control" name="ongkir" value="0" min="0"/>
														</div>

														<div class="row" style="bottom: 0;">
															<div class="col-lg-7 grid-margin text-center">
																<h4 class="card-title text-primary mb-3">Total Bayar</h4>
																<h2 id="total_bayar"> 0 </h2>
															</div>
															<div class="col-lg-3 grid-margin">
																<h4 class="card-title text-primary text-center mb-3">Status Bayar</h4>
																<div class="form-radio">
																	<label class="form-check-label">
																		<input type="radio" class="form-check-input" name="membershipRadios" value="Belum Lunas" checked>
																		Lunas
																	</label>
																</div>
																<div class="form-radio">
																	<label class="form-check-label">
																		<input id="status" type="radio" class="form-check-input" name="membershipRadios" value="Belum Lunas"> Belum Lunas
																	</label>
																</div>
															</div>
															<div class="col-lg-2 grid-margin text-center mt-5">
																<button id="simpan" type="submit" class="btn btn-success mr-2">Simpan</button>
															</div>
														</div>
													</div>
												</div>
											<!-- </form> -->
										</div>
										<!-- Akhir Tambah Order -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- content-wrapper ends -->
				<!-- partial:partials/_footer.html -->
				<footer class="footer">
					<div class="container-fluid clearfix">
						<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
							<a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
						<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
							<i class="mdi mdi-heart text-danger"></i>
						</span>
					</div>
				</footer>
				<!-- partial -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<!-- plugins:js -->
	<script src="<?php echo base_url() ?>asset2/vendors/js/vendor.bundle.base.js"></script>
	<script src="<?php echo base_url() ?>asset2/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- Plugin js for this page-->
	<!-- End plugin js for this page-->
	<!-- inject:js -->
	<script src="<?php echo base_url() ?>asset2/js/off-canvas.js"></script>
	<script src="<?php echo base_url() ?>asset2/js/misc.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script src="<?php echo base_url() ?>asset2/js/dashboard.js"></script>
	<!-- End custom js for this page-->
	<script>
		var result = ''

		function produk(no, gambar_produk, nama_produk, harga_jual, stok, total, kopro, kapro, u) {
			return `
        <tr class="text-center">
          <td> ${no} </td>
          <td class="py-1">
          <!-- <img src="<?php echo base_url() ?>asset2/images/Produk/${gambar_produk}" alt="image" /> -->
          </td>
          <td> ${nama_produk}</td>
          <td> ${harga_jual} </td>
          <td> <input type="number" class="produk form-control" value="0" min="0" max="${stok}" name="qty"/></td>
          <td class='total_harga_barang'> ${total} </td>
          <td>
            <input id='kode_produk' type='hidden' value='${kopro}'>
            <input id='kategori_produk' type='hidden' value='${kapro}'>
            <input id='foto_produk' type='hidden' value='${gambar_produk}'>
            <input id='ukuran' type='hidden' value='${u}'>
            
            <button type="button" class="btn social-btn btn-social-outline-facebook" style="padding:5px; margin-right:5px;" data-toggle="tooltip" title="Hapus">
            <i class="mdi mdi-delete"></i>
          </td>
        </tr>
        `
		}

    $(document.body).on('change keyup', '.produk', function () {
      var total = $(this).closest('td').prev('td').text() * $(this).val()
      $(this).closest('td').next('td').text(total)

      var totalbayar = 0
      $('.total_harga_barang').each(function(){
        totalbayar += parseFloat($(this).html())
      })
      console.log(parseFloat($('#ongkir').val()))
      $('#total_bayar').html(totalbayar + parseFloat($('#ongkir').val()))
    });

    $('#ongkir').keyup(function (e) { 
      var totalbayar = 0
      $('.total_harga_barang').each(function(){
        totalbayar += parseFloat($(this).html())
      })
      $('#total_bayar').html(totalbayar + parseFloat($('#ongkir').val()))
    });

		$('#search_produk').on('keyup', function (event) {
			var search_keyword = $(this).val()
			$.ajax({
				type: "POST",
				url: "search_produk",
				data: {
					keyword: search_keyword
				},
				success: function (response) {
					if (response != "") {
						response = JSON.parse(response)
						response.forEach(function (val, index) {
							result += produk(index + 1, val.gambar_produk, val.nama_produk,
								val.harga_jual, val.stok, '0', val.kode_produk, val.kategori_produk, val.ukuran)
						})
					} else {
						result =
							`
                <tr class="text-center">
                  <td colspan="7"> Kosong </td>
                </tr>
                `
					}

					if (result == "") {
						result =
							`
                <tr class="text-center">
                  <td colspan="7"> Kosong </td>
                </tr>
                `
					}
					$('#hasil_search_tp').html(result)
					result = ""
				},
				error: function (err) {
					console.log(err)
				}
			})
		})

    $('#simpan').click(function (e) { 
      var kuantitas = 0 
      $('.produk').each(function(lol){
        kuantitas += Number($(this).val())
      })
      // var produk = $(".total_harga_barang").filter(function() {
      //     return $(this).text() != 0;
      // })
      $.ajax({
        type: "POST",
        url: "tambah_order",
        data: {
          nama_customer: $('#nama_customer').val(),
          kode_reseller: 'null',
          jenis: 'null',
          notlp: $('#notlp').val(),
          alamat: $('#alamat').val(),
          tgl_order: $('#tgl_order').val(),
          kode_produk: $('#kode_produk').val(),
          kategori_produk: $('#kategori_produk').val(),
          foto_produk: $('#foto_produk').val(),
          ukuran: $('#ukuran').val(),
          kuantitas: kuantitas,
          total_bayar: $('#total_bayar').html(),
          status: $('#status').val(),
        },
        success: function (response) {
          alert('Success');
          location.reload();
        },
        error: function (err) {
          console.log(err)
        },
      });
    });
	</script>
</body>

</html>
