<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Profil | JB</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url() ?>asset2/images/favicon.png" />
  <link rel="stylesheet" href="<?php echo base_url() ?>asset2/vendors/iconfonts/font-awesome/css/font-awesome.css">
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <img src="<?php echo base_url() ?>asset2/images/logo.svg" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="<?php echo base_url() ?>asset2/images/logo-mini.svg" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">

        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
              <span class="count">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 3 new notifications
                </p>
                <span class="badge badge-pill badge-warning float-right">View all</span>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="mdi mdi-alert-circle-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Stock Habis!</h6>
                  <p class="font-weight-light small-text">
                    Stock Sweater Pull & Bear sudah habis!
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="mdi mdi-comment-text-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Stock Hampir Habis!</h6>
                  <p class="font-weight-light small-text">
                    Stock Dress Zara Hampir habis!
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="mdi mdi-email-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Barang Belum Dikirim!</h6>
                  <p class="font-weight-light small-text">
                    Ada 7 orderan belum dikirim!
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, Rizka Febriani !</span>
              <img class="img-xs rounded-circle" src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item mt-2">
                My Profile
              </a>
              <a class="dropdown-item">
                Setting
              </a>
              <a class="dropdown-item">
                Logout
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Rizka Febriani</p>
                  <div>
                    <small class="designation text-muted">Karyawan</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dashboard">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="order">
              <i class="menu-icon mdi mdi-cart"></i>
              <span class="menu-title">Order</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="produk">
              <i class="menu-icon mdi mdi-tshirt-crew"></i>
              <span class="menu-title">Produk</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="laporan">
              <i class="menu-icon mdi mdi-chart-line"></i>
              <span class="menu-title">Laporan Keuangan</span>
            </a>
          </li>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#setting" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-settings"></i>
              <span class="menu-title">Setting</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="setting">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link menu-icon mdi mdi-account" href="profil"> &nbsp; Profil </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link menu-icon mdi mdi-logout" href="logout.php"> &nbsp; Logout </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h2 class="card-title text-primary mb-4">Edit Profil Karyawan</h2>

                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-12" style="padding-left:0%;">
                        <div class="form-group row">
                          <img src="<?php echo base_url() ?>asset2/images/faces/face2.jpg" id='img-upload'/>
                          <span class="input-group-btn">
                            <span class="btn btn-default btn-file">
                              Pilih Gambar <input type="file" id="imgInp">
                            </span>
                          </span>
                        </div>
                      </div>

                      <div class="form-group col-md-6">
                        <label class="form-label">NIK</label>
                        <input type="number" class="form-control" name="nama_produk"/>
                        <label class="form-label">No Tlp</label>
                        <input type="number" class="form-control" name="ukuran" min="0"/>
                        <label class="form-label">Username</label>
                        <input type="text" class="form-control" name="ukuran"/>
                      </div>

                      <div class="form-group col-md-6">
                        <label class="form-label">Nama Lengkap</label>
                        <input type="text" class="form-control" name="nama_produk"/>
                        <label class="form-label">Email</label>
                        <input type="text" class="form-control" name="ukuran"/>
                        <label class="form-label">Password</label>
                        <input type="password" class="form-control" name="ukuran"/>
                      </div>

                      <div class="form-group col-md-12">
                        <label for="exampleTextarea1">Alamat</label>
                        <textarea class="form-control" rows="2" name="alamat"></textarea> <br>
                        <button type="submit" class="btn btn-success mr-2">Simpan</button>
                      </div>
                    </div>
                  </div>

              </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo base_url() ?>asset2/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url() ?>asset2/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url() ?>asset2/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>asset2/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url() ?>asset2/js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>
