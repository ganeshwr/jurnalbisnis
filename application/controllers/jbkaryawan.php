<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jbkaryawan extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		//$this->load->helper('url');
		//$this->load->helper('form');
		$this->load->model('AuthModel');
		$this->load->library('form_validation');
	}


	public function index()
	{
		$this->load->view('view_landingpage');
	}

	public function dashboard()
	{
		$this->load->view('karyawan/view_dashboard');
	}


//--------------------------START SCRIPT lOGIN-------------------------------//

	public function login()
	{
		$this->load->view('view_login');
	}

	public function proses_login()
	{

			$username = $this->input->post['username'];
			$password = $this->input->post['password'];

		if ($this->AuthModel->login($username,$password)){

			$this->load->view('view_produk');

		} else {
			$this->load->view('view_login');
		}
}

//--------------------------END SCRIPT lOGIN-------------------------------//

//--------------------------START SCRIPT PRODUK-------------------------------//

	public function produk()
	{
		$data['data_kategori'] = $this->AuthModel->tampil_kategori()->result();
		$data['data_produk'] = $this->AuthModel->tampil_produk()->result();
		$this->load->view('karyawan/view_produk', $data);
	}

	public function proses_produk()
	{
		$dataproduk = [
			'kode_produk' => $this->input->post('kode_produk'),
			'nama_produk' => $this->input->post('nama_produk'),
			'gambar_produk' => $this->input->post('gambar_produk'),
			'kategori_produk' => $this->input->post('kategori_produk'),
			'ukuran' => $this->input->post('ukuran'),
			'harga_beli' => $this->input->post('harga_beli'),
			'harga_jual' => $this->input->post('harga_jual'),
			'harga_jual_reseller' => $this->input->post('harga_jual_reseller'),
			'stok' => $this->input->post('stok'),
			'keterangan' => $this->input->post('keterangan'),
			'tanggal' => $this->input->post('tanggal'),
		];

		$insert = $this->AuthModel->produk($dataproduk);

		if ($insert){
				redirect('jbkaryawan/produk');
		}
		else {
			echo 'gagal';
		}
	}

	public function hapus_produk($id)
    {
        $this->AuthModel->hapusDataPro($id);
				redirect('jbkaryawan/produk');
    }

	public function detailProduk($id)
    {
        $data['judul'] = 'Detail Produk';
        $data['data_produk'] = $this->AuthModel->getProdukById($id);
				$this->load->view('karyawan/view_produk', $data);
    }

//--------------------------END SCRIPT PRODUK-------------------------------//

//--------------------------START SCRIPT KATEGORI-------------------------------//

	public function proses_kategori()
	{
		$datakategori = [
			'nama_kategori' => $this->input->post('nama_kategori')
		];

		$insert = $this->AuthModel->kategori($datakategori);

		if ($insert){
			redirect('jbkaryawan/produk');
		}
		else {
			echo 'gagal';
		}
	}

	public function hapus_kategori($id)
    {
        $this->AuthModel->hapusDataKat($id);
				redirect('jbkaryawan/produk');
    }

//------------------------ENDT SCRIPT KATEGORI-------------------------------//

//--------------------------START SCRIPT RESELLER-------------------------------//

	public function reseller()
	{
		$this->load->view('karyawan/view_reseller');
	}

	public function proses_reseller()
	{
		$datareseller = [
			'nama_reseller' => $this->input->post('nama_reseller'),
			'alamat' => $this->input->post('alamat'),
			'notlp' => $this->input->post('notlp'),
			'domisili' => $this->input->post('domisili'),

		];

		$insert = $this->AuthModel->reseller($datareseller);

		if ($insert){
			echo 'Sukses';
		}
		else {
			echo 'gagal';
		}
	}

	//--------------------------END SCRIPT RESELLER-------------------------------//

	//--------------------------START SCRIPT ORDER-------------------------------//

	public function order()
	{
		$this->load->view('karyawan/view_order');
	}

	//--------------------------END SCRIPT ORDER-------------------------------//

	//--------------------------START SCRIPT PROFIL-------------------------------//

	public function profil()
	{
		$this->load->view('karyawan/view_profil');
	}

	//--------------------------END SCRIPT ORDER-------------------------------//

	//--------------------------START SCRIPT RESELLER-------------------------------//

	public function laporan()
	{
		$data['data_produk'] = $this->AuthModel->tampil_produk()->result();
		$this->load->view('karyawan/view_laporan', $data);
	}

	//--------------------------START SCRIPT ORDER-------------------------------//

	public function search_produk(){
		$keyword = $this->input->post('keyword');

		if($keyword != ''){
			$result = $this->AuthModel->cari_produk($keyword)->result();
		
			echo json_encode($result);
		}
		else {
			echo '';
		}
	}

	public function tambah_order(){
		$dataorder = [
			'nama_customer' => $this->input->post('nama_customer'),
			'kode_reseller' => $this->input->post('kode_reseller'),
			'jenis' => $this->input->post('jenis'),
			'notlp' => $this->input->post('notlp'),
			'alamat' => $this->input->post('alamat'),
			'tgl_order' => $this->input->post('tgl_order'),
			'kode_produk' => $this->input->post('kode_produk'),
			'kategori_produk' => $this->input->post('kategori_produk'),
			'foto_produk' => $this->input->post('foto_produk'),
			'ukuran' => $this->input->post('ukuran'),
			'kuantitas' => $this->input->post('kuantitas'),
			'total_bayar' => $this->input->post('total_bayar'),
			'status' => $this->input->post('status'),
		];

		$insert = $this->AuthModel->dataorder($dataorder);
	}
	
	//--------------------------END SCRIPT ORDER-------------------------------//

//--------------------------START SCRIPT LAPORAN-------------------------------//
}
