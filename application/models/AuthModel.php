<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthModel extends CI_Model {



	public function register($data) {

		return $this->db->insert('user_owner', $data);
	}

	public function login ($username,$password)
	{
		return $this->db->get_where('user_owner', array('username'=>$username, 'password'=>$password)); //md5($password)
	}

//--------------------------START SCRIPT PRODUK-------------------------------//

	public function produk($dataproduk) {

		return $this->db->insert('data_produk', $dataproduk);
	}

	public function tampil_produk(){
		return $this->db->get('data_produk'); //sama saja dengan select from
	}

	public function hapusDataPro($id)
	{
			// $this->db->where('data_produk',$id);
			$this->db->delete('data_produk', ['id' => $id]);
	}

	public function getProdukById($id)
    {
        return $this->db->get_where('data_produk', ['id' => $id])->row_array();
    }

//------------------------ENDT SCRIPT PRODUK-------------------------------//

//------------------------START SCRIPT KATEGORI-------------------------------//

	public function kategori($datakategori) {

		return $this->db->insert('data_kategori', $datakategori);
	}

	public function tampil_kategori(){

		return $this->db->get('data_kategori'); //sama saja dengan select from
	}

	public function hapusDataKat($id)
	{
			// $this->db->where('nrp',$id);
			$this->db->delete('data_kategori', ['id' => $id]);
	}

	//------------------------END SCRIPT KATEGORI-------------------------------//

//------------------------START SCRIPT RESELLER------------------------------//

	public function reseller($datareseller) {

		return $this->db->insert('data_reseller', $datareseller);
	}

//------------------------END SCRIPT RESELLER-------------------------------//

//------------------------START SCRIPT SUPPLIER-------------------------------//

	public function supplier($datasupplier) {

		return $this->db->insert('data_supplier', $datasupplier);
	}

//------------------------END SCRIPT SUPPLIER-------------------------------//



 // Fungsi untuk melakukan proses upload file
 public function upload(){
	 $config['upload_path'] = './images/'; // utk menentukan tempat gambar yg kita uplooad
	 $config['allowed_types'] = 'jpg|png|jpeg'; // utk menentukan file yg boleh diuplod
	 $config['max_size']  = '2048'; // mx ukuran file
	 $config['remove_space'] = TRUE; // berfungsi utk mengubah spasi menjadi "_" pada nama gambr

	 $this->load->library('upload', $config); // Load konfigurasi uploadnya
	 if($this->upload->do_upload('input_gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
		 // Jika berhasil :
		 $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => ''); // upload->data() utk mengambil data hasil upload yg kita lakukan
		 return $return;
	 }else{
		 // Jika gagal :
		 $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
		 return $return;
	 }
 }

 // Fungsi untuk menyimpan data ke database
  public function save($upload){
    $data = array(
      'kode_produk'=>$this->input->post('input_deskripsi'),
      'nama_produk' => $upload['file']['file_name'],
      'gambar_produk' => $upload['file']['file_size'],
      'kategori_produk' => $upload['file']['file_type'],
      'ukuran' => $upload['file']['file_type'],
      'harga_beli' => $upload['file']['file_type'],
      'harga_jual' => $upload['file']['file_type'],
      'harga_jual_reseller' => $upload['file']['file_type'],
      'stok' => $upload['file']['file_type'],
      'keterangan' => $upload['file']['file_type'],
      'tanggal' => $upload['file']['file_type']
    ); // file $data akan disimpan ke db

    $this->db->insert('data_produk', $data); //insert (nama tabel,array_data)

}
	// search produk (tambah order)
	public function cari_produk($query){
		$this->db->select("*");
		$this->db->from("data_produk");
		$this->db->like('nama_produk', $query);

		return $this->db->get();
	}

	// insert data order
	public function dataorder($data){
		return $this->db->insert('data_order', $data);
	}

} // TUTUP CLASS AUTHMODEL
